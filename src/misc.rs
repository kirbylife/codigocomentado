extern crate tera;

use rand::Rng;
use tera::Context;

const TITLES: [&str; 11] = [
    "/* {} */",
    "# {}",
    "// {}",
    "<!-- {} -->",
    "{# {} #}",
    "-- {}",
    "--[[ {} --]]",
    "; {}",
    "% {}",
    "{- {} -}",
    "(* {} *)",
];

pub fn get_context() -> Context {
    let mut context = Context::new();

    let mut rng = rand::thread_rng();
    let title_fmt = TITLES[rng.gen_range(0, TITLES.len())];
    let title = str::replace(title_fmt, "{}", "CódigoComentado");

    context.insert("title", &title);
    context
}
